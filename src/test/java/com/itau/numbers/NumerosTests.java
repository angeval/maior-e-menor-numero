package com.itau.numbers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class NumerosTests {
    private int[] listaTest;
    Algorithm algorithm = new Algorithm();
    Resultado resultado = new Resultado();

    @Test
    public void testFindBiggerSmallerRightOrder() {
        resultado = algorithm.findBiggerSmaller(new int[] {1, 2, 3});
        Assertions.assertEquals(3, resultado.getBigger());
        Assertions.assertEquals(1, resultado.getSmaller());
    }

    @Test
    public void testFindBiggerSmallerInverseOrder() {
        resultado = algorithm.findBiggerSmaller(new int[] {3, 2, 1});
        Assertions.assertEquals(3, resultado.getBigger());
        Assertions.assertEquals(1, resultado.getSmaller());
    }

    @Test
    public void testFindBiggerSmallerAleatorio() {
        resultado = algorithm.findBiggerSmaller(new int[] {13, 2, 19, 0});
        Assertions.assertEquals(19, resultado.getBigger());
        Assertions.assertEquals(0, resultado.getSmaller());
    }

    @Test
    public void testFindBiggerSmallerAllEquals() {
        resultado = algorithm.findBiggerSmaller(new int[] {1, 1});
        Assertions.assertEquals(1, resultado.getBigger());
        Assertions.assertEquals(1, resultado.getSmaller());
    }

    @Test
    public void testFindBiggerSmallerAllEqualsZero() {
        resultado = algorithm.findBiggerSmaller(new int[] {0, 0, 0, 0});
        Assertions.assertEquals(0, resultado.getBigger());
        Assertions.assertEquals(0, resultado.getSmaller());
    }
}
