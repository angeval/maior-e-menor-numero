package com.itau.numbers;

public class Algorithm {
    public Resultado findBiggerSmaller(int[] listaNumeros) {
        Integer maior = Integer.MIN_VALUE;
        Integer menor = Integer.MAX_VALUE;
        Resultado resultado = new Resultado();

        for (int n: listaNumeros) {
            if(n > maior){
                maior = n;
            }
            if(n<menor){
                menor = n;
            }
        }
        resultado.setBigger(maior);
        resultado.setSmaller(menor);
        return resultado;
    }
}
