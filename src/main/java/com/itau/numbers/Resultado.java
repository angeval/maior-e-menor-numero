package com.itau.numbers;

public class Resultado {

    private int bigger;
    private int smaller;

    public Resultado() {
    }

    public int getBigger() {
        return bigger;
    }

    public void setBigger(int bigger) {
        this.bigger = bigger;
    }

    public int getSmaller() {
        return smaller;
    }

    public void setSmaller(int smaller) {
        this.smaller = smaller;
    }

}
